# kappa-drive-mount

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

- [About](#about)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`kappa-drive-mount` enables you to mount a `kappa-drive` with [`hyperdrive-fuse`](https://github.com/andrewosh/hyperdrive-fuse/).

## Install

TODO

## Usage

This will mount a kappa-drive for 3 seconds, and then unmount it.

```js
const KappaDrive = require('kappa-drive')
const mount = require('./')

const mountdir = './mnt'
const storage = './drive'
const drive = KappaDrive(storage)

mount(drive, mountdir, (err, unmount) => {
  if (err) return console.error(err)

  setTimeout(() => {
    unmount()
  }, 3000)
})
```

## API
```js
const mount = require('kappa-drive-mount')
mount(drive, destination, [options,] callback)
```

mount a kappa-drive, `drive` at path `destination`. `options`, if given, will be passed to `hyperdrive-fuse`.

`callback` will be given arguments `(err, unmount)`.  `unmount` is a function which can be used to unmount the drive.  The drive can also be unmounted by sending the `'SIGINT'` event (pressing Ctrl + C).


## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

TODO
