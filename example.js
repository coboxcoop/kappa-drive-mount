const KappaDrive = require('kappa-drive')
const mount = require('./')

const mountdir = './mnt'
const storage = './drive'
const drive = KappaDrive(storage)

mount(drive, mountdir, (err, unmount) => {
  if (err) return console.error(err)

  setTimeout(() => {
    unmount()
  }, 3000)
})
