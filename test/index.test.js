const { describe } = require('tape-plus')
const ram = require('random-access-memory')
const fs = require('fs')
const mount = require('../')
const KappaDrive = require('kappa-drive')
const path = require('path')

// TODO: test for readdir inside an empty directory - this fails, but passes with normal hyperdrive

const { cleanup, replicate, tmp, run } = require('./util')

describe('mount', (context) => {
  context('read mounted directory', (assert, next) => {
    var drive = KappaDrive(ram)
    var storage = tmp()

    drive.ready(() => {
      drive.writeFile('/hello.txt', 'world', (err) => {
        assert.error(err, 'no error')
        mount(drive, storage, (err, unmount) => {
          assert.error(err, 'no error when mounting')
          fs.readdir(storage, (err, files) => {
            assert.error(err, 'no error')
            assert.same(files, ['hello.txt'], 'readdir returns the written file')
            fs.readFile(path.join(storage, 'hello.txt'), 'utf-8', (err, data) => {
              assert.error(err, 'no error')
              assert.same(data, 'world', 'the file has the correct content')
              unmount(() => {
                cleanup(storage, next)
              })
            })
          })
        })
      })
    })
  })

  context('write to mounted directory', (assert, next) => {
    var drive = KappaDrive(ram)
    var storage = tmp()

    drive.ready(() => {
      mount(drive, storage, (err, unmount) => {
        assert.error(err, 'no error when mounting')
        fs.writeFile(path.join(storage, 'hello.txt'), 'world', (err) => {
          assert.error(err, 'no error')
          drive.readdir('/', (err, files) => {
            assert.error(err, 'no error')
            assert.same(files, ['hello.txt'], 'drive.readdir returns the written file')
            unmount(() => {
              cleanup(storage, next)
            })
          })
        })
      })
    })
  })

  context('mkdir in mounted directory', (assert, next) => {
    var drive = KappaDrive(ram)
    var storage = tmp()
    drive.ready(() => {
      mount(drive, storage, (err, unmount) => {
        assert.error(err, 'no error when mounting')
        fs.mkdir(path.join(storage, 'antelopes'), (err) => {
          assert.error(err, 'no error')
          drive.readdir('/', (err, files) => {
            assert.error(err, 'no error')
            assert.same(files, ['antelopes'], 'drive.readdir returns directory name')
            unmount(() => {
              cleanup(storage, next)
            })
          })
        })
      })
    })
  })

  context('readdir in mounted newly created directory', (assert, next) => {
    var drive = KappaDrive(ram)
    var storage = tmp()
    drive.ready(() => {
      mount(drive, storage, (err, unmount) => {
        assert.error(err, 'no error when mounting')
        fs.mkdir(path.join(storage, 'antelopes'), (err) => {
          assert.error(err, 'no error')
          fs.readdir(path.join(storage, 'antelopes'), (err, files) => {
            assert.error(err, 'no error')
            assert.same(files, [], 'drive.readdir returns empty array for empty directory')
            fs.writeFile(path.join(storage, 'antelopes', 'tree.txt'), 'flim', (err) => {
              assert.error(err, 'no error')
              fs.readdir(path.join(storage, 'antelopes'), (err, files) => {
                assert.error(err, 'no error')
                assert.same(files, ['tree.txt'], 'drive.readdir returns directory name')
                unmount(() => {
                  cleanup(storage, next)
                })
              })
            })
          })
        })
      })
    })
  })

  context('ls using readdir in mounted subdirectory', (assert, next) => {
    var drive = KappaDrive(ram)
    var storage = tmp()
    drive.ready(() => {
      mount(drive, storage, (err, unmount) => {
        assert.error(err, 'no error when mounting')
        run(`echo "what noise does a dog make?" > ${storage}/hello.txt`, (err, output) => {
          assert.error(err, 'no error on write')
          run(`ls ${storage}`, (err, output) => {
            assert.error(err, 'no error on read')
            run(`mkdir ${storage}/bark`, (err, output) => {
              assert.error(err, 'no error on write of sub-directory')
              run(`ls ${storage}/bark`, (err, output) => {
                assert.error(err, 'no error on read empty sub-directory')
                assert.same(output, [], 'ls returns empty array for empty directory')

                var filename = 'woof.txt'
                run(`echo "the dog goes woof!" > ${storage}/bark/${filename}`, (err, output) => {
                  assert.error(err, 'no error on write to sub-directory')
                  run(`ls ${storage}/bark`, (err, output) => {
                    assert.error(err, 'no error on read sub-directory')
                    assert.same(output.length, 1, 'returns a single file')
                    assert.same(output[0].replace('\n', ''), filename, 'filename matches')
                    unmount(() => {
                      cleanup(storage, next)
                    })
                  })
                })
              })
            })
          })
        })
      })
    })
  })

  context('cat to write file, then cat to read file', (assert, next) => {
    var drive = KappaDrive(ram)
    var storage = tmp()
    drive.ready(() => {
      mount(drive, storage, (err, unmount) => {
        assert.error(err, 'no error when mounting')
        var content = "woof woof"
        var filename = 'woof.txt'
        run(`echo "${content}" | cat > ${storage}/${filename}`, (err, output) => {
          assert.error(err, 'no error')
          run(`cat ${storage}/${filename}`, (err, output) => {
            assert.error(err, 'no error')
            assert.same(output.length, 1, 'returns some content')
            assert.same(output[0].replace('\n', ''), content, 'content matches')
            unmount(() => {
              cleanup(storage, next)
            })
          })
        })
      })
    })
  })

  context('mv/rename file', (assert, next) => {
    var drive = KappaDrive(ram)
    var storage = tmp()
    drive.ready(() => {
      mount(drive, storage, (err, unmount) => {
        assert.error(err, 'no error when mounting')
        var content = 'what noise does a dog make?'
        run(`echo "${content}" > ${storage}/hello.txt`, (err) => {
          assert.error(err, 'no error on write')
          run(`mv ${storage}/hello.txt ${storage}/goodbye.txt`, (err) => {
            assert.error(err, 'no error on mv')
            run(`cat ${storage}/goodbye.txt`, (err, output) => {
              assert.error(err, 'no error on read')
              assert.same(output.length, 1, 'returns some content')
              assert.same(output[0].replace('\n', ''), content, 'content matches')
              unmount(() => {
                cleanup(storage, next)
              })
            })
          })
        })
      })
    })
  })

  context('unlink in mounted directory', (assert, next) => {
    var drive = KappaDrive(tmp())
    var storage = tmp()
    drive.ready(() => {
      mount(drive, storage, (err, unmount) => {
        assert.error(err, 'no error when mounting')
        fs.writeFile(path.join(storage, 'tree.txt'), 'is this a tree', (err) => {
          assert.error(err, 'no error')
          fs.readdir(storage, (err, files) => {
            assert.error(err, 'no error')
            assert.same(files, ['tree.txt'], 'readdir')
            fs.unlink(path.join(storage, 'tree.txt'), (err) => {
              assert.error(err, 'no error')
              fs.readdir(storage, (err, files) => {
                assert.error(err, 'no error')
                assert.same(files, [], 'drive.readdir returns nothing')
                unmount(() => {
                  cleanup(storage, next)
                })
              })
            })
          })
        })
      })
    })
  })

  context('replicate into empty drive', (assert, next) => {
    var feedsOnDisk = {
      orange: tmp(),
      blue: tmp()
    }
    var orangeDrive = KappaDrive(feedsOnDisk.orange)
    var blueDrive = KappaDrive(feedsOnDisk.blue)

    var storage = {}
    var unmount = {}

    function setup (drive, colour, cb) {
      storage[colour] = tmp()
      mount(drive, storage[colour], (err, unmountFn) => {
        assert.error(err, 'no error when mounting')
        unmount[colour] = unmountFn
        drive.ready(cb)
      })
    }

    setup(orangeDrive, 'orange', function () {
      setup(blueDrive, 'blue', function () {
        fs.writeFile(path.join(storage.orange, 'tree.txt'), 'is this a tree', (err) => {
          assert.error(err, 'no error on write')
          replicate(orangeDrive, blueDrive, check)
        })
      })
    })

    function check (err) {
      assert.error(err, 'no error on replicate')
      fs.readdir(storage.blue, (err, files) => {
        assert.error(err, 'no error on readdir')
        assert.same(files, ['tree.txt'], 'fs.readdir returns file')
        unmount.orange(() => {
          unmount.blue(() => {
            cleanup(Object.values(feedsOnDisk), next)
          })
        })
      })
    }
  })

  context("replicate - append to another peer's file - using unix commands", (assert, next) => {
    var feedsOnDisk = {
      orange: tmp(),
      blue: tmp()
    }
    var orangeDrive = KappaDrive(feedsOnDisk.orange)
    var blueDrive = KappaDrive(feedsOnDisk.blue)

    var storage = {}
    var unmount = {}

    function setup (drive, colour, cb) {
      storage[colour] = tmp()
      mount(drive, storage[colour], (err, unmountFn) => {
        assert.error(err, 'no error when mounting')
        unmount[colour] = unmountFn
        drive.ready(cb)
      })
    }

    setup(orangeDrive, 'orange', function () {
      setup(blueDrive, 'blue', function () {
        run(`echo "what noise does a dog make?" > ${storage.orange}/hello.txt`, (err, output) => {
          assert.error(err, 'no error on write')
          // 'seed' the blue drive with a file (TODO this is a hack)
          run(`echo "this file is not needed" > ${storage.blue}/blue.txt`, (err, output) => {
            assert.error(err, 'no error on echo to file')
            replicate(orangeDrive, blueDrive, check)
          })
        })
      })
    })

    function check (err) {
      assert.error(err, 'no error on replicate')
      run(`ls ${storage.blue}`, (err, output) => {
        assert.error(err, 'no error on ls')
        assert.true(output[0].split('\n').indexOf('hello.txt') > -1, 'file successfully replicated')
        run(`echo "WOOF" >> ${storage.blue}/hello.txt`, (err, output) => {
          assert.error(err, 'no error on echo append to file')
          blueDrive.readFile(`${storage.blue}/hello.txt`, (err, data) => {
            run(`cat ${storage.blue}/hello.txt`, (err, output) => {
              assert.error(err, 'no error on cat out file')
              assert.same(output[0], 'what noise does a dog make?\nWOOF\n', 'woof has correctly been appended')
              replicate(orangeDrive, blueDrive, (err) => {
                assert.error(err, 'no error on replicate')
                // run(`ls -l ${storage.orange}`, err output)
                run(`cat ${storage.orange}/hello.txt`, (err, output) => {
                  assert.error(err, 'no error on cat out file')
                  assert.same(output, ['what noise does a dog make?\nWOOF\n'], 'woof has correctly replicated')
                  unmount.orange(() => {
                    unmount.blue(() => {
                      cleanup(Object.values(feedsOnDisk), next)
                    })
                  })
                })
              })
            })
          })
        })
      })
    }
  })

  context("replicate - unlink another peer's file - using unix commands", (assert, next) => {
    var feedsOnDisk = {
      orange: tmp(),
      blue: tmp()
    }
    var orangeDrive = KappaDrive(feedsOnDisk.orange)
    var blueDrive = KappaDrive(feedsOnDisk.blue)

    var storage = {}
    var unmount = {}

    function setup (drive, colour, cb) {
      storage[colour] = tmp()
      mount(drive, storage[colour], (err, unmountFn) => {
        assert.error(err, 'no error when mounting')
        unmount[colour] = unmountFn
        drive.ready(cb)
      })
    }

    setup(orangeDrive, 'orange', function () {
      setup(blueDrive, 'blue', function () {
        run(`echo "what noise does a dog make?" > ${storage.orange}/hello.txt`, (err, output) => {
          assert.error(err, 'no error on write')
          // 'seed' the blue drive with a file (TODO this is a hack)
          run(`echo "this file is not needed" > ${storage.blue}/blue.txt`, (err, output) => {
            assert.error(err, 'no error on echo to file')
            replicate(orangeDrive, blueDrive, check)
          })
        })
      })
    })

    function check (err) {
      assert.error(err, 'no error on replicate')
      run(`ls ${storage.blue}`, (err, output) => {
        assert.error(err, 'no error on ls')
        assert.true(output[0].split('\n').indexOf('hello.txt') > -1, 'file successfully replicated')
        run(`unlink ${storage.blue}/hello.txt`, (err, output) => {
          assert.error(err, 'no error on rm file')
          run(`cat ${storage.blue}/hello.txt`, (err, output) => {
            assert.ok(err, 'error on cat out file')
            assert.notOk(output, 'cat returns no output')
            replicate(orangeDrive, blueDrive, (err) => {
              assert.error(err, 'no error on replicate')
              run(`ls ${storage.orange}`, (err, output) => {
                assert.error(err, 'no error')
                // TODO this line should also use .split:
                assert.ok(output.indexOf('hello.txt\n') < 0, 'ls does not find the file')
                unmount.orange(() => {
                  unmount.blue(() => {
                    cleanup(Object.values(feedsOnDisk), next)
                  })
                })
              })
            })
          })
        })
      })
    }
  })

  context("replicate - move/rename another peer's file - using unix commands", (assert, next) => {
    var feedsOnDisk = {
      orange: tmp(),
      blue: tmp()
    }
    var orangeDrive = KappaDrive(feedsOnDisk.orange)
    var blueDrive = KappaDrive(feedsOnDisk.blue)

    var storage = {}
    var unmount = {}

    function setup (drive, colour, cb) {
      storage[colour] = tmp()
      mount(drive, storage[colour], (err, unmountFn) => {
        assert.error(err, 'no error when mounting')
        unmount[colour] = unmountFn
        drive.ready(cb)
      })
    }

    setup(orangeDrive, 'orange', function () {
      setup(blueDrive, 'blue', function () {
        run(`echo "what noise does a dog make?" > ${storage.orange}/hello.txt`, (err, output) => {
          assert.error(err, 'no error on write')
          // 'seed' the blue drive with a file (TODO this is a hack)
          run(`echo "this file is not needed" > ${storage.blue}/blue.txt`, (err, output) => {
            assert.error(err, 'no error on echo to file')
            replicate(orangeDrive, blueDrive, check)
          })
        })
      })
    })

    function check (err) {
      assert.error(err, 'no error on replicate')
      run(`ls ${storage.blue}`, (err, output) => {
        assert.error(err, 'no error on ls')
        assert.true(output[0].split('\n').indexOf('hello.txt') > -1, 'file successfully replicated')
        run(`mv ${storage.blue}/hello.txt ${storage.blue}/goodbye.txt`, (err, output) => {
          assert.error(err, 'no error on mv file')
          run(`cat ${storage.blue}/hello.txt`, (err, output) => {
            assert.ok(err, 'error on cat out original file')
            assert.notOk(output, 'cat returns no output')
            replicate(orangeDrive, blueDrive, (err) => {
              assert.error(err, 'no error on replicate')
              run(`ls ${storage.orange}`, (err, output) => {
                assert.error(err, 'no error')
                assert.ok(output[0].split('\n').indexOf('hello.txt') < 0, 'ls does not find the original file')
                assert.ok(output[0].split('\n').indexOf('goodbye.txt') > -1, 'ls does find the renamed file')
                unmount.orange(() => {
                  unmount.blue(next)
                })
              })
            })
          })
        })
      })
    }
  })

  context("replicate - rmdir another peer's dir - using unix commands", (assert, next) => {
    var feedsOnDisk = {
      orange: tmp(),
      blue: tmp()
    }
    var orangeDrive = KappaDrive(feedsOnDisk.orange)
    var blueDrive = KappaDrive(feedsOnDisk.blue)

    var storage = {}
    var unmount = {}

    function setup (drive, colour, cb) {
      storage[colour] = tmp()
      mount(drive, storage[colour], (err, unmountFn) => {
        assert.error(err, 'no error when mounting')
        unmount[colour] = unmountFn
        drive.ready(cb)
      })
    }

    setup(orangeDrive, 'orange', function () {
      setup(blueDrive, 'blue', function () {
        run(`mkdir ${storage.orange}/cabbage`, (err, output) => {
          assert.error(err, 'no error on mkdir')
          // 'seed' the blue drive with a file (TODO this is a hack)
          run(`echo "this file is not needed" > ${storage.blue}/blue.txt`, (err, output) => {
            assert.error(err, 'no error on echo to file')
            replicate(orangeDrive, blueDrive, check)
          })
        })
      })
    })

    function check (err) {
      assert.error(err, 'no error on replicate')
      run(`ls ${storage.blue}`, (err, output) => {
        assert.error(err, 'no error on ls')
        assert.true(output[0].split('\n').indexOf('cabbage') > -1, 'dir successfully replicated')
        run(`rmdir ${storage.blue}/cabbage`, (err, output) => {
          assert.error(err, 'no error on rmdir')
          run(`ls ${storage.blue}`, (err, output) => {
            assert.error(err, 'error on ls')
            assert.false(output[0].split('\n').indexOf('cabbage') > -1, 'ls no longer lists dir')
            replicate(orangeDrive, blueDrive, (err) => {
              assert.error(err, 'no error on replicate')
              run(`ls ${storage.orange}`, (err, output) => {
                assert.error(err, 'no error')
                assert.false(output[0].split('\n').indexOf('cabbage') > -1, 'ls no longer lists dir')
                unmount.orange(() => {
                  unmount.blue(next)
                })
              })
            })
          })
        })
      })
    }
  })

  // TODO: to be resolved...
  //
  // context("replicate - append to another peer's empty file - using unix commands", (assert, next) => {
  //   var feedsOnDisk = {
  //     orange: tmp(),
  //     blue: tmp()
  //   }
  //   var orangeDrive = KappaDrive(feedsOnDisk.orange)
  //   var blueDrive = KappaDrive(feedsOnDisk.blue)

  //   var storage = {}
  //   var unmount = {}

  //   function setup (drive, colour, cb) {
  //     storage[colour] = tmp()
  //     mount(drive, storage[colour], (err, unmountFn) => {
  //       assert.error(err, 'no error when mounting')
  //       unmount[colour] = unmountFn
  //       drive.ready(cb)
  //     })
  //   }

  //   setup(orangeDrive, 'orange', function () {
  //     setup(blueDrive, 'blue', function () {
  //       run(`touch ${storage.orange}/hello.txt`, (err, output) => {
  //         assert.error(err, 'no error on touch command')
  //         // 'seed' the blue drive with a file (TODO this is a hack)
  //         run(`echo "this file is not needed" >> ${storage.blue}/blue.txt`, (err, output) => {
  //           assert.error(err, 'no error on echo append to file')
  //           replicate(orangeDrive, blueDrive, check)
  //         })
  //       })
  //     })
  //   })

  //   function check (err) {
  //     assert.error(err, 'no error on replicate')
  //     run(`ls ${storage.blue}`, (err, output) => {
  //       assert.error(err, 'no error on ls')
  //       assert.true(output[0].split('\n').indexOf('hello.txt') > -1, 'file successfully replicated')
  //       run(`echo "WOOF" >> ${storage.blue}/hello.txt`, (err, output) => {
  //         assert.error(err, 'no error on echo append to file')
  //         run(`cat ${storage.blue}/hello.txt`, (err, output) => {
  //           assert.error(err, 'no error on cat out file')
  //           assert.same(output[0], 'WOOF\n', 'woof has correctly been written')
  //           replicate(orangeDrive, blueDrive, (err) => {
  //             assert.error(err, 'no error on replicate')
  //             // run(`ls -l ${storage.orange}`, err output)
  //             run(`cat ${storage.orange}/hello.txt`, (err, output) => {
  //               assert.error(err, 'no error on cat out file')
  //               assert.same(output, ['WOOF\n'], 'woof has correctly replicated')
  //               unmount.orange(() => {
  //                 unmount.blue(next)
  //               })
  //             })
  //           })
  //         })
  //       })
  //     })
  //   }
  // })

  // TODO: This test fails, edge case to be resolved
  //
  // context('https://ledger-git.dyne.org/CoBox/kappa-drive/issues/4', (assert, next) => {
  //   var feedsOnDisk = {
  //     yellow: tmp(),
  //     purple: tmp()
  //   }

  //   var yellowDrive = KappaDrive(feedsOnDisk.yellow)
  //   var purpleDrive = KappaDrive(feedsOnDisk.purple)

  //   var storage = {}
  //   var unmount = {}

  //   function setup (drive, colour, cb) {
  //     storage[colour] = tmp()
  //     mount(drive, storage[colour], (err, unmountFn) => {
  //       assert.error(err, 'no error when mounting')
  //       unmount[colour] = unmountFn
  //       drive.ready(cb)
  //     })
  //   }

  //   setup(yellowDrive, 'yellow', function () {
  //     setup(purpleDrive, 'purple', function () {
  //       fs.open(path.join(storage.yellow, 'hello.txt'), 'w+', (err, fd) => {
  //         assert.error(err, 'no error')
  //         fs.write(fd, Buffer.from('world'), 0, 5, (err) => {
  //           assert.error(err, 'no error')
  //           fs.close(fd, (err) => {
  //             assert.error(err, 'no error')

  //             fs.writeFile(path.join(storage.purple, 'seedfile.txt'), 'oink', (err) => {
  //               assert.error(err, 'no error')

  //               replicate(yellowDrive, purpleDrive, (err) => {
  //                 assert.error(err, 'no error')

  //                 fs.open(path.join(storage.purple, 'hello.txt'), 'r+', (err, fd) => {
  //                   assert.error(err, 'no error')
  //                   assert.ok(fd, 'returns a file descriptor')

  //                   var readBuf = Buffer.alloc('world'.length)
  //                   fs.read(fd, readBuf, 0, 5, null, (err, bytesRead) => {
  //                     assert.error(err, 'no error')
  //                     assert.same(bytesRead, 5, 'read correct number of bytes')
  //                     assert.same(readBuf, Buffer.from('world'), 'successfully reads replicated file')

    //                     fs.write(fd, Buffer.from('mundo'), 0, 5, (err) => {
    //                       assert.error(err, 'no error')
  //                       fs.close(fd, (err) => {
  //                         assert.error(err, 'no error')

    //                         replicate(yellowDrive, purpleDrive, (err) => {
  //                           assert.error(err, 'no error')
  //                           fs.readFile(path.join(storage.yellow, 'hello.txt'), 'utf-8', (err, data) => {
  //                             assert.error(err, 'no error')
  //                             assert.same(data, 'mundo', 'data corrently overwritten and replicted')
  //                             unmount.yellow(() => {
  //                               unmount.purple(next)
  //                             })
  //                           })
  //                         })
  //                       })
  //                     })
  //                   })
  //                 })
  //               })
  //             })
  //           })
  //         })
  //       })
    //     })
  //   })
  // })
})
