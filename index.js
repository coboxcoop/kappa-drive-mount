const rimraf = require('rimraf')
const { HyperdriveFuse } = require('hyperdrive-fuse')
const mkdirp = require('mkdirp')
const maybe = require('call-me-maybe')
const debug = require('debug')
const Fuse = require('fuse-native')

module.exports = function mount (drive, dir, opts, cb) {
  const log = opts.logger || debug('kappa-drive-mount')

  if ((typeof opts === 'function') && !cb) return mount(drive, dir, {}, opts)

  return maybe(cb, new Promise((resolve, reject) => {
    mkdirp.sync(dir)

    const hyperfuse = new HyperdriveFuse(drive, dir, opts)

    var handlers = hyperfuse.getBaseHandlers()
    var mounting = hyperfuse.mount(handlers)

    mounting.catch((err) => {
      log({ msg: 'failed to mount hyperdrive-fuse', err })
      return reject(err)
    })

    mounting.then((driveObject) => {
      process.on('SIGINT', done)
      return resolve(done)
    })

    function done (callback) {
      if (typeof callback !== 'function' || !callback) callback = noop

      return maybe(callback, new Promise((resolve, reject) => {
        hyperfuse.unmount().then(() => {
          process.removeListener('SIGINT', done)
          rimraf(dir, (err) => {
            if (err) {
              log({ msg: 'failed to remove directory', err })
              return reject(err)
            }
            return resolve()
          })
        }).catch((err) => {
          log({ msg: 'failed to unmount hyperdrive-fuse', err })
          throw err
        })
      }))
    }
  }))
}

function noop () {}
